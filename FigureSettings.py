# -*- coding: utf-8 -*-
"""
Settings
DOT Plot Library

12/08/2020
Thomas Haex
"""

import matplotlib.pyplot as plt

def SetSettings():
    'Plot settings'
    #plt.rc('text', usetex=True)
    #plt.rc('font', family='serif', serif='Times')
    plt.rc('xtick', labelsize=10)
    plt.rc('ytick', labelsize=10)
    plt.rc('axes', labelsize=10, titlesize=11)
    plt.rc('legend', fontsize=9)
    plt.rc('axes', axisbelow=True)

    Plots = {}
    Plots["width"] = 12/2.54
    Plots["height"] = Plots["width"]/1.618
    Plots["Color"] = [[0/255,59/255,91/255], [231/255,103/255,39/255], [30/255,118/255,129/255], [229/255,170/255,18/255],
                 [197/255,197/255,197/255], [69/255,115/255,48/255], [156/255,27/255,42/255], [86/255,86/255,86/255],
                 [132/255,96/255,21/255], [192/255,217/255,223/255], [155/255,185/255,71/255]]
    Plots["LineWidthA"] = [1.5, 1.5, 1.5, 1.5, 1.5]
    Plots["LineStyleA"] = ['-', '-', '-', '-', '-']
    Plots["LineWidthB"] = [1.5, 1.5, 1.5, 1.5, 2.2]
    Plots["LineStyleB"] = ['-', '-.', '--', ':', '-']
    Plots["MarkerStyle"] = ['o', 'v', 's', 'p', 'X']
    
    return plt, Plots