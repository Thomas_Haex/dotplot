# -*- coding: utf-8 -*-
"""
Plot
DOT Plot Library

17/08/2020
Thomas Haex
"""

from dotplot import FigureSettings as S

'Initialze settings'
plt, Plots = S.SetSettings()

def SingleVariable(X, Y, xLabel, yLabel, Title = None, Label = None,
                     xMin = None, xMax = None, yMin = None, yMax = None, Colour = None):
    """
    Arguments: [X, Y, xLabel, yLabel]
    Optional Arguments: [Title, Label, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    if Colour == None:
        plt.plot(X,Y,color=Plots["Color"][0], linewidth=Plots["LineWidthB"][0], linestyle=Plots["LineStyleB"][0], label=Label)
    else:
        plt.plot(X,Y,color=Plots["Color"][0], linewidth=Plots["LineWidthA"][0], linestyle=Plots["LineStyleA"][0], label=Label)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    if Title != None:
        plt.title(Title)
    if Label != None:
        plt.legend(loc = 'best')
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)  
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()
    
def DoubleVariable(X, Y1, Y2, xLabel, yLabel, LineLabel1, LineLabel2, Title = None,
                     xMin = None, xMax = None, yMin = None, yMax = None, Colour = None):
    """
    Arguments: [X, Y1, Y2, xLabel, yLabel, lineLabel1, lineLabel2]
    Optional Arguments: [Title, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    if Colour == None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthB"][0], linestyle=Plots["LineStyleB"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthB"][1], linestyle=Plots["LineStyleB"][1], label=LineLabel2)
    else:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthA"][0], linestyle=Plots["LineStyleA"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthA"][1], linestyle=Plots["LineStyleA"][1], label=LineLabel2)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend(loc = 'best')
    if Title != None:
        plt.title(Title)
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()

def TripleVariable(X, Y1, Y2, Y3, xLabel, yLabel, LineLabel1, LineLabel2, LineLabel3, Title = None,
                     xMin = None, xMax = None, yMin = None, yMax = None, Colour = None):
    """
    Arguments: [X, Y1, Y2, Y3, xLabel, yLabel, lineLabel1, lineLabel2, LineLabel3]
    Optional Arguments: [Title, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    if Colour == None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthB"][0], linestyle=Plots["LineStyleB"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthB"][1], linestyle=Plots["LineStyleB"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthB"][2], linestyle=Plots["LineStyleB"][2], label=LineLabel3)
    else:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthA"][0], linestyle=Plots["LineStyleA"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthA"][1], linestyle=Plots["LineStyleA"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthA"][2], linestyle=Plots["LineStyleA"][2], label=LineLabel3)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend(loc = 'best')
    if Title != None:
        plt.title(Title)
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)  
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()

def QuadrupelVariable(X, Y1, Y2, Y3, Y4, xLabel, yLabel, LineLabel1, LineLabel2, LineLabel3, LineLabel4,
                          Title = None, xMin = None, xMax = None, yMin = None, yMax = None, Colour = None):
    """
    Arguments: [X, Y1, Y2, Y3, Y4, xLabel, yLabel, lineLabel1, lineLabel2, LineLabel3, LineLabel4]
    Optional Arguments: [Title, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    if Colour == None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthB"][0], linestyle=Plots["LineStyleB"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthB"][1], linestyle=Plots["LineStyleB"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthB"][2], linestyle=Plots["LineStyleB"][2], label=LineLabel3)
        plt.plot(X,Y4,color=Plots["Color"][3], linewidth=Plots["LineWidthB"][3], linestyle=Plots["LineStyleB"][3], label=LineLabel4)
    if Colour != None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthA"][0], linestyle=Plots["LineStyleA"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthA"][1], linestyle=Plots["LineStyleA"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthA"][2], linestyle=Plots["LineStyleA"][2], label=LineLabel3)
        plt.plot(X,Y4,color=Plots["Color"][3], linewidth=Plots["LineWidthA"][3], linestyle=Plots["LineStyleA"][3], label=LineLabel4)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend(loc = 'best')
    if Title != None:
        plt.title(Title)
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)  
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()

def QuintupleVariable(X, Y1, Y2, Y3, Y4, Y5, xLabel, yLabel, LineLabel1, LineLabel2, LineLabel3, LineLabel4,
                      LineLabel5, Title = None, xMin = None, xMax = None, yMin = None, yMax = None, Colour = None):
    """
    Arguments: [X, Y1, Y2, Y3, Y4, Y5, xLabel, yLabel, lineLabel1, lineLabel2, LineLabel3, LineLabel4, LineLabel5]
    Optional Arguments: [Title, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    if Colour == None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthB"][0], linestyle=Plots["LineStyleB"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthB"][1], linestyle=Plots["LineStyleB"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthB"][2], linestyle=Plots["LineStyleB"][2], label=LineLabel3)
        plt.plot(X,Y4,color=Plots["Color"][3], linewidth=Plots["LineWidthB"][3], linestyle=Plots["LineStyleB"][3], label=LineLabel4)
        plt.plot(X,Y5,color=Plots["Color"][4], linewidth=Plots["LineWidthB"][4], linestyle=Plots["LineStyleB"][4], label=LineLabel5)
    if Colour != None:
        plt.plot(X,Y1,color=Plots["Color"][0], linewidth=Plots["LineWidthA"][0], linestyle=Plots["LineStyleA"][0], label=LineLabel1)
        plt.plot(X,Y2,color=Plots["Color"][1], linewidth=Plots["LineWidthA"][1], linestyle=Plots["LineStyleA"][1], label=LineLabel2)
        plt.plot(X,Y3,color=Plots["Color"][2], linewidth=Plots["LineWidthA"][2], linestyle=Plots["LineStyleA"][2], label=LineLabel3)
        plt.plot(X,Y4,color=Plots["Color"][3], linewidth=Plots["LineWidthA"][3], linestyle=Plots["LineStyleA"][3], label=LineLabel4)
        plt.plot(X,Y5,color=Plots["Color"][4], linewidth=Plots["LineWidthA"][4], linestyle=Plots["LineStyleA"][4], label=LineLabel5)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend(loc = 'best')
    if Title != None:
        plt.title(Title)
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)  
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()