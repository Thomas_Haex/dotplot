# -*- coding: utf-8 -*-
"""
Bar
DOT Plot Library

17/08/2020
Thomas Haex
"""

from dotplot import FigureSettings as S
import numpy as np

'Initialze settings'
plt, Plots = S.SetSettings()

def ThreeGroups(Means1, Means2, Means3, Means1Label, Means2Label, Means3Label,
                       Labels, yLabel, Title = None, yMax = None, LegendSpace = None):
    x = np.arange(len(Labels))
    width = 0.25
    fig, ax = plt.subplots()
    ax.bar(x - width, Means1, width, label=Means1Label, color=Plots["Color"][0])
    ax.bar(x, Means2, width, label=Means2Label, color=Plots["Color"][1])
    ax.bar(x + width, Means3, width, label=Means3Label, color=Plots["Color"][2])

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel(yLabel)
    ax.set_xticks(x)
    ax.set_xticklabels(Labels)
    ax.legend()
    plt.grid()
    if Title != None:
        ax.set_title(Title)
    ax.set_xlim([-0.5,max(x)+LegendSpace])
    if yMax != None:
        ax.set_ylim([0,yMax])
    fig.set_size_inches(Plots["width"], Plots["height"])
    fig.tight_layout()