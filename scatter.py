# -*- coding: utf-8 -*-
"""
Scatter
DOT Plot Library

17/08/2020
Thomas Haex
"""

from dotplot import FigureSettings as S

'Initialze settings'
plt, Plots = S.SetSettings()

def SingleVariable(X, Y, xLabel, yLabel, Title = None, Label = None,
                     xMin = None, xMax = None, yMin = None, yMax = None, dotSize = None, equal = None):
    """
    Arguments: [X, Y, xLabel, yLabel]
    Optional Arguments: [Title, Label, xMin, xMax, yMin, yMax]
    """
    fig, ax = plt.subplots()
    plt.scatter(X,Y,color=Plots["Color"][0], marker=Plots["MarkerStyle"][0], label=Label, s=dotSize)
    plt.grid()
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    if Title != None:
        plt.title(Title)
    if Label != None:
        plt.legend(loc = 'best')
    if xMin != None:
        plt.xlim(xmin = xMin)
    if xMax != None:
        plt.xlim(xmax = xMax)
    if yMin != None:
        plt.ylim(ymin = yMin)
    if yMax != None:
        plt.ylim(ymax = yMax)  
    if equal != None:
        plt.axis('equal')
    fig.set_size_inches(Plots["width"], Plots["height"])
    plt.tight_layout()